module Releases
  module EntityDefinitions

    class ReleaseEntity < PowerStencil::SystemEntityDefinitions::ProjectEntity

      DEFAULT_VERSION = '0.0.1'

      entity_type :release

      field :version
      field :content, :is_hash
      has_one :release, name: :parent, with_reverse_method: :children

      def initialize(fields: {}, universe: nil, user: false)
        super
        self.version = DEFAULT_VERSION if version.nil?
      end

      def full_name
        '%s v%s' % [name, version]
      end

      def bump_content(*content_key, content_increment: :patch, release_increment: :patch)
        get_entity(*content_key) do |entity|
          content[entity] = bump_version content[entity], content_increment
        end
        self.version = bump_version version, release_increment
      rescue => e
        raise PowerStencil::Error, "Cannot bump_content because '#{e.message}'"
      end

      def bump(level: :patch)
        self.version = bump_version version, level
      end

      def specifically_valid?(raise_error: false)
        content.each do |entity, version|
          unless entity.is_a? UniverseCompiler::Entity::Base
            return false_or_raise"Invalid hash in release content of #{as_path} ! Key should be an entity !", raise_error: raise_error
          end
          unless PowerStencil::Utils::SemanticVersion.valid_version?(version, raise_error: false)
            return false_or_raise"Invalid version '#{version}' defined for #{entity.as_path} in release #{as_path} !", raise_error: raise_error
          end
        end
        true
      end

      def save(uri = source_uri, raise_error: true, force_save: false, force_files_generation: false )
        self.content = content.map { |entity, version| [entity, version.to_s] }.to_h
        super
      end

      private

      def get_entity(*entity_criteria, &block)
        case entity_criteria.size
        when 1
          case entity_criteria.first
          when UniverseCompiler::Entity::Base
            entity = entity_criteria.first
            yield entity

          when Array
            entity_criteria.first.each do |entity|
              yield entity
            end

          when String
            if md = entity_criteria.first.match(/^\s*(?<type>[^\/]+)\/(?<name>.+)\s*$/)
              search_type = md[:type].to_sym
              search_name = md[:name]
              entity = project_engine.entity search_type, search_name, project_engine.root_universe
              yield entity
            else
              raise PowerStencil::Error, 'Invalid entity search ID'
            end

          else
            raise PowerStencil::Error, 'Invalid entity search_criteria'
          end

        when 2
          entity = project_engine.entity *entity_criteria, project_engine.root_universe
          yield entity

        else
          raise PowerStencil::Error, 'Invalid entity search_criteria number'
        end

      end

      def bump_version(a_version, level)
        cur_version =  PowerStencil::Utils::SemanticVersion.new a_version
        cur_version.increment(level).to_s
      end


    end

  end
end
