module Releases
  module Dsl

    module ReleasesDsl

      def target_release?
        return false if project.config[:release].nil?
        return false if target_release(raise_error: false).nil?

        true
      end

      def target_release(raise_error: true)
        if project.config[:release].nil?
          project.logger.warn "Call to DSL method 'target_release', but no target release specified in the config."
          return nil
        end
        rel = entity PowerStencil.config[:release]
        false_or_raise "Invalid release '#{project.config[:release]}' !", raise_error: raise_error if rel.nil?
        false_or_raise "'#{project.config[:release]}' is not a release (this is a '#{rel.type}') !", raise_error: raise_error unless rel.type == :release
        rel
      end

    end

  end
end
