module Releases
  module PluginHelper

    def plugin_templates_path
      File.join plugin_root_path, 'etc', 'templates'
    end

    def command_line_definition_file_path
      File.join plugin_root_path, 'etc', 'command_line.yaml'
    end

    def plugin_root_path
      File.expand_path File.join('..', '..', '..'), __FILE__
    end

  end
end
