
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'releases/version'

Gem::Specification.new do |spec|
  spec.name          = 'psplugin_releases'
  spec.version       = Releases::VERSION
  spec.authors       = ['Laurent Briais']
  spec.email         = ['powerstencil-contact@brizone.org']

  spec.summary       = %q{releases is a plugin for the power_stencil framework.}
  spec.description   = %q{Release provides entities to manage releases, ie consistent sets of ... something.}
  spec.homepage      = 'https://gitlab.com/tools4devops/psplugins/releases'
  spec.license       = 'MIT'

  # Change this and following metadata if you don't want your plugin to be an "official" PowerStencil plugin.
  # ie deployed to https://gitlab.com/tools4devops/psplugins
  source_code_uri = 'https://gitlab.com/tools4devops/psplugins/releases'

  # Gem metadata
  if spec.respond_to?(:metadata)
    # Nice link to your home page on rubygems.org
    spec.metadata['homepage_uri'] = spec.homepage

    # You have to probably change this if you don't deploy to gitlab
    spec.metadata['bug_tracker_uri'] = "#{source_code_uri}/issues"
    spec.metadata['documentation_uri'] = "#{source_code_uri}/blob/master/README.md"
    spec.metadata['source_code_uri'] = source_code_uri

    # This metadata is mandatory for a PowerStencil plugin !!
    spec.metadata['plugin_name'] = 'releases'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes and PowerStencil plugin mechanism !'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'power_stencil', '~> 0.9.4'
end
