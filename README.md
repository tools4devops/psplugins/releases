Releases
========

[![Gem Version](https://badge.fury.io/rb/psplugin_releases.svg)](https://rubygems.org/gems/psplugin_releases) [![Pipeline status](https://gitlab.com/tools4devops/psplugins/releases/badges/master/pipeline.svg)](https://gitlab.com/tools4devops/psplugins/releases/commits/master)

[PowerStencil] is the Swiss-army knife templating workflow for developers and ops.

`Releases` is a [PowerStencil] plugin.

See [official website][PowerStencil site].


<!-- TOC -->

- [What is a PowerStencil plugin ?](#what-is-a-powerstencil-plugin-)
- [Using this plugin in your `PowerStencil` projects](#using-this-plugin-in-your-powerstencil-projects)
- [Goal of this plugin](#goal-of-this-plugin)
- [Plugin dependencies](#plugin-dependencies)
- [Plugin capabilities](#plugin-capabilities)
    - [Subcommands and options](#subcommands-and-options)
    - [DSL in templates and in `power_stencil shell`](#dsl-in-templates-and-in-power_stencil-shell)
    - [Entity types](#entity-types)
        - [`release`](#release)
- [Contributing](#contributing)
    - [License](#license)
    - [Code of Conduct](#code-of-conduct)

<!-- /TOC -->

# What is a PowerStencil plugin ?

A `PowerStencil` plugin can be used in the context of a `PowerStencil` project and bring extra features to a standard project.

If you don't know what `PowerStencil` is made for, you may read [the documentation][PowerStencil] first.

Features provided by a plugin can be:

* Extra config.
* Extra subcommands or options added to the `power_stencil` command-line.
* Extra post-build actions.
* Extra DSL methods available in templates and in `power_stencil shell`.
* Extra entity types.
* Extra templates-templates.

# Using this plugin in your `PowerStencil` projects

To use this plugin within your `PowerStencil` project, you need to:

* be within a `PowerStencil` project :wink:.
* declare this plugin in the project configuration (from the root of your project in the `.ps_project/versioned-config.yaml` file).

In this yaml configuration file, you just have to add a new entry in the `:project_plugins` array or simply create it if it is not yet existing:

```yaml
:project_plugins:
  - psplugin_releases
```
If this plugin is not already present on your machine, you may have to download it:

    $ power_stencil plugin --install

And then you may see information about the plugin by running:

* `power_stencil info` in the plugins section.
* `power_stencil plugin --list -v`

# Goal of this plugin

The goal of this plugin is to be able to build anything you can already build within your `PowerStencil` project but in the context of a `release`.
You can then use the information provided by the `release` within your build templates.

# Plugin dependencies

There is no external dependency for this plugin.

# Plugin capabilities

This plugin provides the following features:

## Subcommands and options

This does not bring any new subcommand. It just adds an option to the `power_stencil build` subcommand:

* `--release`

which expects a entity ID of the type `release` to be provided.

For example:

    $ power_stencil build a_buildable_entity_type/an_entity --release release/my_release

## DSL in templates and in `power_stencil shell`

This plugin brings release-related methods to the `PowerStencil` DSL, available in both templates and in the `power_stencil shell`:

* `target_release(raise_error: true)` returns the release passed on the command line to the `build` sub-command, or `nil` if no release was provided [on the command line](#subcommands-and-options) (or raise an error).
* `target_release?` will simply say if a release was provided on the command-line, without raising any error.


## Entity types


### `release`

The idea is that a `release` has a `version` (following semantic versioning principles) and a `content`.

`content` is a hash which keys are entities and values are versions (following semantic versioning principles). `content` represents obviously the content of a `release`.

On top of this it brings:
* `bump(level: :patch)` will bump the version of the release, following semantic versioning rules. `:patch` is the default, but could be `:minor`, or `:major` (see `PowerStencil::Utils::SemanticVersion::MANDATORY_LEVELS`).
* `bump_content(*content_key, content_increment: :patch, release_increment: :patch)`, a convenient methods to manage the version of each of the content entities version as well as the global release version. By default, it will bump the versions of all entities "_passed_" to the method (ie found from `content_key`), and them bump the version of the release itself.
  * `content_key`, can be an entity ID, a type and a name, or even an entity or an array of entities.
  * `content_increment` and `release_increment` define how to  increase versions. 
* inheritance between releases themself. Each release being able to have one upstream and multiple downstream releases.
  * One release can have a `parent` release.
  * One release can have multiple `children` releases.

:information_source: Use `release` to keep consistency between an arbitrary number of entities which may have version requirements on each others.

# Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/tools4devops/psplugins/releases/issues. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License].

## Code of Conduct

Everyone interacting in the PowerStencil project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct].

<!-- End of Document -->

<!-- Pages -->
[code of conduct]: CODE_OF_CONDUCT.md

<!-- Code links -->

<!-- Illustrations -->
[simple-flow-image]: doc/images/power-stencil-simple-flow.svg

<!-- External links -->
[MIT License]: http://opensource.org/licenses/MIT "The MIT license"
[ERB]: https://ruby-doc.org/stdlib-2.6.3/libdoc/erb/rdoc/ERB.html "Quick ERB description"
[Haml]: http://haml.info/ "The templating engine for XML-like documents"
[Ruby]: https://www.ruby-lang.org "The powerful Ruby language"
[Rails]: https://rubyonrails.org/ "The Ruby on Rails framework"
[PowerStencil site]: https://powerstencil.brizone.org "Official PowerStencil website"
[PowerStencil]: https://gitlab.com/tools4devops/power_stencil/blob/master/README.md "PowerStencil documentation"
[PowerStencil plugins]: https://gitlab.com/tools4devops/power_stencil/blob/master/doc/plugins.md "PowerStencil plugins documentation"
