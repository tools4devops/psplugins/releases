RSpec.describe Releases do

  include PowerStencilTests::Project
  temporary_project 'test_project', scope: :all

  let(:build_help_cmd) { 'build --help' }
  let(:test_entity) { 'release/test_release' }
  let(:create_release) { "create #{test_entity}" }
  let(:get_release) { "get #{test_entity}" }

  it 'has a version number' do
    expect(Releases::VERSION).not_to be nil
  end

  it 'should add a --release option to `build` sub-command' do
    expect(`#{temporary_project_cmd build_help_cmd}`).to match '--release'
  end

  it 'should be possible to create a release' do
    `#{temporary_project_cmd create_release}`
    expect($?.success?).to be_truthy
    expect(`#{temporary_project_cmd get_release}`).not_to be_empty
  end

end
